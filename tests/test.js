import chakram from 'chakram';
import exec from 'ssh-exec';
import config from '../data/data';
import sleep from 'thread-sleep';

// currently harcoding values, we will pass VM and SNAPSHOT throught args or we will get it from mongoDB
const VM = process.env.VM;
const SNAPSHOT = process.env.SNAPSHOT;
const IP = process.env.IP;
const con = process.env.con;

const fs = require('fs');
const request = require('request');
const readline = require('readline');
const {google} = require('googleapis');

var contains = require("string-contains");

// If modifying these scopes, delete credentials.json.```
const SCOPES = ['https://www.googleapis.com/auth/drive'];
const TOKEN_PATH = 'token.json';

// Google Drive API 
const key = require('../gdriveapi.json'); // JSON file which contains Google Service Account Credentials
const drive = google.drive('v3');

let expect = chakram.expect;
describe("Backend Test", function () {
    console.log(VM);
    console.log(SNAPSHOT);
    console.log(IP);
    // currently harcoding values and commeting test which will get it to not wait it each time
    // TODO: delete values and uncomment tests
    let VMid = 178;
    let snap = 1;
    var vm_id;
    var ss_id;
    var clean_ss_id;
    var revert_to_ss_id;
    var creds;
    var configuration;
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

    switch (con) {
        case "personal":
            configuration = config.personalEsxiAuth;
            break;
        case "automation":
            configuration = config.automationEsxiAuth;
            break;
    }

     it("should connect to esxi host and find a VM ID",  (done) => {
         exec('vim-cmd vmsvc/getallvms',
         configuration,
         function (err, stdout, stderr) {
                expect(err).to.be.null;
                expect(stdout).to.be.a('string');
                expect(stderr).to.be.empty;
                stdout.split('\n').forEach(line => {
                    if(contains(line, VM)){
                        vm_id = line.split(' ')[0];
                        console.log("id of vm to revert: " + vm_id + "\n");
                    }
                });
                 done();
        });
    });
//
     it("should find a VM's shapshoot Id", (done) => {
         exec("vim-cmd vmsvc/snapshot.get " + vm_id,
         configuration,
         function (err, stdout, stderr) {
                 expect(err).to.be.null;
                 expect(stdout).to.be.a('string');
                 expect(stderr).to.be.empty;
                var lines = stdout.split('\n');
                for (var i = 0; i < lines.length; i++) {
                    if(contains(lines[i], 'Snapshot Name')) {
                        var snapshot_name = lines[i].split(':')[1].trim();
                        var snapshot_id = lines[i+1].split(':')[1].trim();
                        if(snapshot_name == SNAPSHOT) {
                            revert_to_ss_id = snapshot_id;
                            console.log("id of snapshot to revert to: " + revert_to_ss_id + "\n");
                        }
                    }
                }
                 done();
               });
     });

     it("should revert VM's shapshoot", (done) => {
         exec("vim-cmd  vmsvc/snapshot.revert " + vm_id + " " + revert_to_ss_id + " 1",
         configuration,
         function (err, stdout, stderr) {
                 expect(err).to.be.null;
                 expect(stdout).to.have.string('Revert Snapshot:');
                 expect(stderr).to.be.empty;
                 done();
               });
     });

     it("should turn on VM", (done) => {
         setTimeout(() => {
         exec("vim-cmd  vmsvc/power.on " + vm_id,
         configuration,
          (err, stdout, stderr) => {
             console.log(stdout);
                 expect(err).to.be.null;
                 expect(stdout).to.have.string('Powering on VM');
                 expect(stderr).to.be.empty;
                 done();
               });
             }, 2000);
     });

     it("VM should have Powered on status", (done) => {
         exec("vim-cmd  vmsvc/power.getstate " + vm_id,
         configuration,
         (err, stdout, stderr) => {
                 expect(err).to.be.null;
                 expect(stdout).to.have.string('Powered on');
                 expect(stderr).to.be.empty;
                 done();
               });
     });

     it("wait to Admin stie be UP", async () =>{
         let ok = false;
         let again = 0;
         do {
             try {
                let res = await chakram.get("http://" + IP + ":4242/healthstats/");
                expect(res.error).to.be.null;
                 return true;
             } catch (err) {
                 console.log("TRY AGAIN");
                 console.log(err);
                 again++;
                 if (again > 5) {
                     ok = true;
                     expect(again).to.not.be.above(5);
                     return false;
                 } else {
                     sleep(2000);
                 }
             }
         } while (!ok)
     });

//    it("download update file from G drive", (done)=>{
//        const jwtClient = new google.auth.JWT(
//            key.client_email,
//            null,
//            key.private_key,
//            [   'https://www.googleapis.com/auth/drive.file',
//                'https://www.googleapis.com/auth/drive',
//                'https://www.googleapis.com/auth/drive.metadata'
//            ],
//            null
//            );
//
//
//        jwtClient.authorize((authErr) => {
//            if (authErr) {
//                console.log(authErr);
//                return;
//            }else{
//                console.log("Google Client Authorization Successful!")
//            }
//
//            var id = '1FQKp9QI9Lb54El32DZLwCu6mPowgbrP3';
//            drive.files.list({auth: jwtClient}, (listErr, resp) => {
//                if (listErr) {
//                    console.log(listErr);
//                    return;
//                }
//                if (resp != null) {
//                    resp.data.files.forEach((file) => {
//                        if (file.mimeType == "text/plain") {
//                          var dest = fs.createWriteStream(file.name + ".txt");
//                          request(config.GdriveDownloadLink + file.id).pipe(dest);
//                        }
//                    });
//                }else{
//                    console.log("Response is null.")
//                    return;
//                }
//            });
//
//             drive.files.get({
//                 fileId: id,
//                 alt: 'media'
//             }, function(err, result) {
//                 console.log(result); // Binary file content here
//                 console.log(err)
//             });
//        });
//        done();
//    });
});